import dynamic from 'next/dynamic';

const DyanmicMap = dynamic(
  () => import('@components/Map'),
  { ssr: false }
);

function HomePage() {
    return (
        <DyanmicMap />
    )
}

export default HomePage