module.exports = {
    rewrites: function(){
        return [
            {
                source: '/@components/:componentName',
                destination: '/@components/:componentName/:componentName'
            }
        ]
    }
}