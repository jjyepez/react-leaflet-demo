exports.ids = [0];
exports.modules = {

/***/ "./components/Map/index.js":
/*!*********************************!*\
  !*** ./components/Map/index.js ***!
  \*********************************/
/*! exports provided: Map, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Map", function() { return Map; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet */ "leaflet");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet/dist/leaflet.css */ "./node_modules/leaflet/dist/leaflet.css");
/* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-leaflet */ "react-leaflet");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_Map_style_module_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @components/Map/style.module.css */ "./components/Map/style.module.css");
/* harmony import */ var _components_Map_style_module_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_Map_style_module_css__WEBPACK_IMPORTED_MODULE_5__);

var _jsxFileName = "C:\\Users\\Desarrollo\\Desktop\\~pers\\code\\misc\\tmp\\bpz-next\\components\\Map\\index.js";





const position = [4.60931910300002, -74.185823456];

async function loadData() {
  return new Promise((resolve, reject) => {
    resolve(Array(5).fill([]).map(item => {
      return {
        position: [position[0] + Math.random() / 10, position[1] + Math.random() / 10]
      };
    }));
  });
}

const Map = () => {
  let {
    0: markers,
    1: setMarkers
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(async () => {
    let markers = await loadData();
    console.log({
      markers
    });
    setMarkers(markers);
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["MapContainer"], {
    className: _components_Map_style_module_css__WEBPACK_IMPORTED_MODULE_5___default.a.map,
    center: position,
    zoom: 12,
    maxZoom: 18,
    scrollWheelZoom: true,
    tileSize: 512,
    fadeAnimation: false,
    nocrs: leaflet__WEBPACK_IMPORTED_MODULE_2__["CRS"].EPSG4326,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["TileLayer"], {
      attribution: "\xA9 <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
      nourl: "http://mt3.google.com/vt/lyrs=r&x={x}&y={y}&z={z}",
      url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 13
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Circle"], {
      center: position,
      radius: 2000,
      fillColor: "red",
      fillOpacity: .15,
      weight: 3,
      color: "red",
      opacity: ".75"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 13
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Polyline"], {
      color: "red",
      stroke: true,
      weight: 5,
      opacity: .5,
      smoothFactor: 21,
      positions: markers.sort((a, b) => b[0] > a[0]).map(position => position.position)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 13
    }, undefined), markers.length > 0 && markers.map((marker, i) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
      position: [marker.position[0], marker.position[1]],
      draggable: false,
      icon: new leaflet__WEBPACK_IMPORTED_MODULE_2__["Icon"]({
        iconSize: [30, 40],
        iconAnchor: [15, 40],
        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg',
        popupAnchor: [0, -40]
      })
    }, i, false, {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 17
    }, undefined)), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
      position: position,
      draggable: false,
      icon: new leaflet__WEBPACK_IMPORTED_MODULE_2__["Icon"]({
        iconSize: [30, 40],
        iconAnchor: [15, 40],
        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg',
        popupAnchor: [0, -40]
      }),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
        children: "This is the tooltip"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 17
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Popup"], {
        children: ["A pretty CSS3 popup. ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 103,
          columnNumber: 42
        }, undefined), " Easily customizable."]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 17
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 13
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 31,
    columnNumber: 9
  }, undefined);
};
/* harmony default export */ __webpack_exports__["default"] = (Map);

/***/ }),

/***/ "./components/Map/style.module.css":
/*!*****************************************!*\
  !*** ./components/Map/style.module.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Exports
module.exports = {
	"map": "style_map__1FytI"
};


/***/ }),

/***/ "./node_modules/leaflet/dist/leaflet.css":
/*!***********************************************!*\
  !*** ./node_modules/leaflet/dist/leaflet.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL01hcC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL01hcC9zdHlsZS5tb2R1bGUuY3NzIl0sIm5hbWVzIjpbInBvc2l0aW9uIiwibG9hZERhdGEiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsIkFycmF5IiwiZmlsbCIsIm1hcCIsIml0ZW0iLCJNYXRoIiwicmFuZG9tIiwiTWFwIiwibWFya2VycyIsInNldE1hcmtlcnMiLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsImNvbnNvbGUiLCJsb2ciLCJzdHlsZXMiLCJDUlMiLCJFUFNHNDMyNiIsInNvcnQiLCJhIiwiYiIsImxlbmd0aCIsIm1hcmtlciIsImkiLCJJY29uIiwiaWNvblNpemUiLCJpY29uQW5jaG9yIiwiaWNvblVybCIsInBvcHVwQW5jaG9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBRUEsTUFBTUEsUUFBUSxHQUFHLENBQUMsZ0JBQUQsRUFBbUIsQ0FBQyxZQUFwQixDQUFqQjs7QUFFQSxlQUFlQyxRQUFmLEdBQXlCO0FBQ3JCLFNBQU8sSUFBSUMsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUNwQ0QsV0FBTyxDQUFDRSxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQ0hDLElBREcsQ0FDRSxFQURGLEVBRUhDLEdBRkcsQ0FFQ0MsSUFBSSxJQUFJO0FBQUUsYUFBTztBQUFFUixnQkFBUSxFQUFFLENBQUNBLFFBQVEsQ0FBQyxDQUFELENBQVIsR0FBY1MsSUFBSSxDQUFDQyxNQUFMLEtBQWMsRUFBN0IsRUFBaUNWLFFBQVEsQ0FBQyxDQUFELENBQVIsR0FBY1MsSUFBSSxDQUFDQyxNQUFMLEtBQWMsRUFBN0Q7QUFBWixPQUFQO0FBQXNGLEtBRmpHLENBQUQsQ0FBUDtBQUlILEdBTE0sQ0FBUDtBQU1IOztBQUVNLE1BQU1DLEdBQUcsR0FBRyxNQUFNO0FBQ3JCLE1BQUk7QUFBQSxPQUFDQyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QkMsc0RBQVEsQ0FBQyxFQUFELENBQXBDO0FBRUFDLHlEQUFTLENBQUUsWUFBWTtBQUNuQixRQUFJSCxPQUFPLEdBQUcsTUFBTVgsUUFBUSxFQUE1QjtBQUNBZSxXQUFPLENBQUNDLEdBQVIsQ0FBWTtBQUFDTDtBQUFELEtBQVo7QUFDQUMsY0FBVSxDQUFDRCxPQUFELENBQVY7QUFDSCxHQUpRLEVBSU4sRUFKTSxDQUFUO0FBTUEsc0JBQ0kscUVBQUMsMERBQUQ7QUFDSSxhQUFTLEVBQUVNLHVFQUFNLENBQUNYLEdBRHRCO0FBRUksVUFBTSxFQUFFUCxRQUZaO0FBR0ksUUFBSSxFQUFFLEVBSFY7QUFJSSxXQUFPLEVBQUUsRUFKYjtBQUtJLG1CQUFlLEVBQUUsSUFMckI7QUFNSSxZQUFRLEVBQUUsR0FOZDtBQU9JLGlCQUFhLEVBQUUsS0FQbkI7QUFRSSxTQUFLLEVBQUVtQiwyQ0FBRyxDQUFDQyxRQVJmO0FBQUEsNEJBVUkscUVBQUMsdURBQUQ7QUFDSSxpQkFBVyxFQUFDLDBFQURoQjtBQUVJLFdBQUssRUFBQyxtREFGVjtBQUdJLFNBQUcsRUFBQztBQUhSO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBVkosZUF3QkkscUVBQUMsb0RBQUQ7QUFDSSxZQUFNLEVBQUVwQixRQURaO0FBRUksWUFBTSxFQUFFLElBRlo7QUFHSSxlQUFTLEVBQUMsS0FIZDtBQUlJLGlCQUFXLEVBQUUsR0FKakI7QUFLSSxZQUFNLEVBQUUsQ0FMWjtBQU1JLFdBQUssRUFBQyxLQU5WO0FBT0ksYUFBTyxFQUFDO0FBUFo7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF4QkosZUFrQ0kscUVBQUMsc0RBQUQ7QUFDSSxXQUFLLEVBQUMsS0FEVjtBQUVJLFlBQU0sRUFBRSxJQUZaO0FBR0ksWUFBTSxFQUFFLENBSFo7QUFJSSxhQUFPLEVBQUUsRUFKYjtBQUtJLGtCQUFZLEVBQUUsRUFMbEI7QUFNSSxlQUFTLEVBQUVZLE9BQU8sQ0FBQ1MsSUFBUixDQUFhLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVQSxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUtELENBQUMsQ0FBQyxDQUFELENBQTdCLEVBQWtDZixHQUFsQyxDQUFzQ1AsUUFBUSxJQUFJQSxRQUFRLENBQUNBLFFBQTNEO0FBTmY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFsQ0osRUEyQ01ZLE9BQU8sQ0FBQ1ksTUFBUixHQUFpQixDQUFqQixJQUFzQlosT0FBTyxDQUFDTCxHQUFSLENBQVksQ0FBQ2tCLE1BQUQsRUFBU0MsQ0FBVCxrQkFDaEMscUVBQUMsb0RBQUQ7QUFFSSxjQUFRLEVBQUUsQ0FBQ0QsTUFBTSxDQUFDekIsUUFBUCxDQUFnQixDQUFoQixDQUFELEVBQXFCeUIsTUFBTSxDQUFDekIsUUFBUCxDQUFnQixDQUFoQixDQUFyQixDQUZkO0FBR0ksZUFBUyxFQUFFLEtBSGY7QUFJSSxVQUFJLEVBQUUsSUFBSTJCLDRDQUFKLENBQVM7QUFDWEMsZ0JBQVEsRUFBRSxDQUFDLEVBQUQsRUFBSyxFQUFMLENBREM7QUFFWEMsa0JBQVUsRUFBRSxDQUFDLEVBQUQsRUFBSyxFQUFMLENBRkQ7QUFHWEMsZUFBTyxFQUFFLHNFQUhFO0FBSVhDLG1CQUFXLEVBQUUsQ0FBQyxDQUFELEVBQUksQ0FBQyxFQUFMO0FBSkYsT0FBVDtBQUpWLE9BQ1NMLENBRFQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFEb0IsQ0EzQzVCLGVBeURJLHFFQUFDLG9EQUFEO0FBQ0ksY0FBUSxFQUFFMUIsUUFEZDtBQUVJLGVBQVMsRUFBRSxLQUZmO0FBR0ksVUFBSSxFQUFFLElBQUkyQiw0Q0FBSixDQUFTO0FBQ1hDLGdCQUFRLEVBQUUsQ0FBQyxFQUFELEVBQUssRUFBTCxDQURDO0FBRVhDLGtCQUFVLEVBQUUsQ0FBQyxFQUFELEVBQUssRUFBTCxDQUZEO0FBR1hDLGVBQU8sRUFBRSxzRUFIRTtBQUlYQyxtQkFBVyxFQUFFLENBQUMsQ0FBRCxFQUFJLENBQUMsRUFBTDtBQUpGLE9BQVQsQ0FIVjtBQUFBLDhCQVVJLHFFQUFDLHFEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVZKLGVBY0kscUVBQUMsbURBQUQ7QUFBQSx5REFDeUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF6REo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREo7QUFpRkgsQ0ExRk07QUE0RlFwQixrRUFBZixFOzs7Ozs7Ozs7OztBQ2hIQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiIwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5pbXBvcnQgeyBDUlMsIEljb24gfSBmcm9tICdsZWFmbGV0JztcclxuaW1wb3J0IFwibGVhZmxldC9kaXN0L2xlYWZsZXQuY3NzXCI7XHJcblxyXG5pbXBvcnQgeyBNYXBDb250YWluZXIsIFRpbGVMYXllciwgTWFya2VyLCBQb3B1cCwgQ2lyY2xlLCBQb2x5bGluZSwgVG9vbHRpcCwgV01TVGlsZUxheWVyIH0gZnJvbSAncmVhY3QtbGVhZmxldCc7XHJcblxyXG5pbXBvcnQgc3R5bGVzIGZyb20gJ0Bjb21wb25lbnRzL01hcC9zdHlsZS5tb2R1bGUuY3NzJztcclxuXHJcbmNvbnN0IHBvc2l0aW9uID0gWzQuNjA5MzE5MTAzMDAwMDIsIC03NC4xODU4MjM0NTZdO1xyXG5cclxuYXN5bmMgZnVuY3Rpb24gbG9hZERhdGEoKXtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgcmVzb2x2ZShBcnJheSg1KVxyXG4gICAgICAgICAgICAuZmlsbChbXSlcclxuICAgICAgICAgICAgLm1hcChpdGVtID0+IHsgcmV0dXJuIHsgcG9zaXRpb246IFtwb3NpdGlvblswXSArIE1hdGgucmFuZG9tKCkvMTAsIHBvc2l0aW9uWzFdICsgTWF0aC5yYW5kb20oKS8xMF0gfX0gKVxyXG4gICAgICAgIClcclxuICAgIH0pXHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBNYXAgPSAoKSA9PiB7XHJcbiAgICBsZXQgW21hcmtlcnMsIHNldE1hcmtlcnNdID0gdXNlU3RhdGUoW10pXHJcblxyXG4gICAgdXNlRWZmZWN0KCBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgbGV0IG1hcmtlcnMgPSBhd2FpdCBsb2FkRGF0YSgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHttYXJrZXJzfSk7XHJcbiAgICAgICAgc2V0TWFya2VycyhtYXJrZXJzKTtcclxuICAgIH0sIFtdKVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPE1hcENvbnRhaW5lclxyXG4gICAgICAgICAgICBjbGFzc05hbWU9e3N0eWxlcy5tYXB9XHJcbiAgICAgICAgICAgIGNlbnRlcj17cG9zaXRpb259XHJcbiAgICAgICAgICAgIHpvb209ezEyfVxyXG4gICAgICAgICAgICBtYXhab29tPXsxOH1cclxuICAgICAgICAgICAgc2Nyb2xsV2hlZWxab29tPXt0cnVlfVxyXG4gICAgICAgICAgICB0aWxlU2l6ZT17NTEyfVxyXG4gICAgICAgICAgICBmYWRlQW5pbWF0aW9uPXtmYWxzZX1cclxuICAgICAgICAgICAgbm9jcnM9e0NSUy5FUFNHNDMyNn1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIDxUaWxlTGF5ZXJcclxuICAgICAgICAgICAgICAgIGF0dHJpYnV0aW9uPScmY29weTsgPGEgaHJlZj1cImh0dHA6Ly9vc20ub3JnL2NvcHlyaWdodFwiPk9wZW5TdHJlZXRNYXA8L2E+IGNvbnRyaWJ1dG9ycydcclxuICAgICAgICAgICAgICAgIG5vdXJsPVwiaHR0cDovL210My5nb29nbGUuY29tL3Z0L2x5cnM9ciZ4PXt4fSZ5PXt5fSZ6PXt6fVwiXHJcbiAgICAgICAgICAgICAgICB1cmw9XCJodHRwczovL3tzfS50aWxlLm9wZW5zdHJlZXRtYXAub3JnL3t6fS97eH0ve3l9LnBuZ1wiXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB7LyogPFdNU1RpbGVMYXllclxyXG4gICAgICAgICAgICAgICAgb3BhY2l0eT17LjE1fVxyXG4gICAgICAgICAgICAgICAgdHJhbnNwYXJlbnQ9e3RydWV9XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHVybD0naHR0cDovL293cy5tdW5kaWFsaXMuZGUvc2VydmljZXMvc2VydmljZT8nXHJcbiAgICAgICAgICAgICAgICBsYXllcnM9J1NSVE0zMC1Db2xvcmVkLUhpbGxzaGFkZSdcclxuICAgICAgICAgICAgLz4gKi99XHJcblxyXG4gICAgICAgICAgICA8Q2lyY2xlXHJcbiAgICAgICAgICAgICAgICBjZW50ZXI9e3Bvc2l0aW9ufVxyXG4gICAgICAgICAgICAgICAgcmFkaXVzPXsyMDAwfVxyXG4gICAgICAgICAgICAgICAgZmlsbENvbG9yPSdyZWQnXHJcbiAgICAgICAgICAgICAgICBmaWxsT3BhY2l0eT17LjE1fVxyXG4gICAgICAgICAgICAgICAgd2VpZ2h0PXszfVxyXG4gICAgICAgICAgICAgICAgY29sb3I9J3JlZCdcclxuICAgICAgICAgICAgICAgIG9wYWNpdHk9Jy43NSdcclxuICAgICAgICAgICAgLz5cclxuXHJcbiAgICAgICAgICAgIDxQb2x5bGluZVxyXG4gICAgICAgICAgICAgICAgY29sb3I9J3JlZCdcclxuICAgICAgICAgICAgICAgIHN0cm9rZT17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIHdlaWdodD17NX1cclxuICAgICAgICAgICAgICAgIG9wYWNpdHk9ey41fVxyXG4gICAgICAgICAgICAgICAgc21vb3RoRmFjdG9yPXsyMX1cclxuICAgICAgICAgICAgICAgIHBvc2l0aW9ucz17bWFya2Vycy5zb3J0KChhLCBiKSA9PiBiWzBdPmFbMF0pLm1hcChwb3NpdGlvbiA9PiBwb3NpdGlvbi5wb3NpdGlvbil9XHJcbiAgICAgICAgICAgIC8+XHJcblxyXG4gICAgICAgICAgICB7IG1hcmtlcnMubGVuZ3RoID4gMCAmJiBtYXJrZXJzLm1hcCgobWFya2VyLCBpKSA9PiAoXHJcbiAgICAgICAgICAgICAgICA8TWFya2VyXHJcbiAgICAgICAgICAgICAgICAgICAga2V5PXtpfVxyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uPXtbbWFya2VyLnBvc2l0aW9uWzBdLCBtYXJrZXIucG9zaXRpb25bMV1dfVxyXG4gICAgICAgICAgICAgICAgICAgIGRyYWdnYWJsZT17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbj17bmV3IEljb24oe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uU2l6ZTogWzMwLCA0MF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb25BbmNob3I6IFsxNSwgNDBdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uVXJsOiAnaHR0cHM6Ly91cGxvYWQud2lraW1lZGlhLm9yZy93aWtpcGVkaWEvY29tbW9ucy9lL2VkL01hcF9waW5faWNvbi5zdmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3B1cEFuY2hvcjogWzAsIC00MF1cclxuICAgICAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICkpfVxyXG5cclxuICAgICAgICAgICAgPE1hcmtlclxyXG4gICAgICAgICAgICAgICAgcG9zaXRpb249e3Bvc2l0aW9ufVxyXG4gICAgICAgICAgICAgICAgZHJhZ2dhYmxlPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgIGljb249e25ldyBJY29uKHtcclxuICAgICAgICAgICAgICAgICAgICBpY29uU2l6ZTogWzMwLCA0MF0sXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbkFuY2hvcjogWzE1LCA0MF0sXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvblVybDogJ2h0dHBzOi8vdXBsb2FkLndpa2ltZWRpYS5vcmcvd2lraXBlZGlhL2NvbW1vbnMvZS9lZC9NYXBfcGluX2ljb24uc3ZnJyxcclxuICAgICAgICAgICAgICAgICAgICBwb3B1cEFuY2hvcjogWzAsIC00MF1cclxuICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8VG9vbHRpcD5cclxuICAgICAgICAgICAgICAgICAgICBUaGlzIGlzIHRoZSB0b29sdGlwXHJcbiAgICAgICAgICAgICAgICA8L1Rvb2x0aXA+XHJcblxyXG4gICAgICAgICAgICAgICAgPFBvcHVwPlxyXG4gICAgICAgICAgICAgICAgICAgIEEgcHJldHR5IENTUzMgcG9wdXAuIDxiciAvPiBFYXNpbHkgY3VzdG9taXphYmxlLlxyXG4gICAgICAgICAgICAgICAgPC9Qb3B1cD5cclxuXHJcbiAgICAgICAgICAgIDwvTWFya2VyPlxyXG5cclxuICAgICAgICA8L01hcENvbnRhaW5lcj5cclxuXHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1hcDtcclxuIiwiLy8gRXhwb3J0c1xubW9kdWxlLmV4cG9ydHMgPSB7XG5cdFwibWFwXCI6IFwic3R5bGVfbWFwX18xRnl0SVwiXG59O1xuIl0sInNvdXJjZVJvb3QiOiIifQ==