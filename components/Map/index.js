import React, { useEffect, useState } from 'react';

import { CRS, Icon } from 'leaflet';
import "leaflet/dist/leaflet.css";

import { MapContainer, TileLayer, Marker, Popup, Circle, Polyline, Tooltip, WMSTileLayer } from 'react-leaflet';

import styles from '@components/Map/style.module.css';

const position = [4.60931910300002, -74.185823456];

async function loadData(){
    return new Promise((resolve, reject) => {
        resolve(Array(5)
            .fill([])
            .map(item => { return { position: [position[0] + Math.random()/10, position[1] + Math.random()/10] }} )
        )
    })
}

export const Map = () => {
    let [markers, setMarkers] = useState([])

    useEffect( async () => {
        let markers = await loadData();
        console.log({markers});
        setMarkers(markers);
    }, [])

    return (
        <MapContainer
            className={styles.map}
            center={position}
            zoom={12}
            maxZoom={18}
            scrollWheelZoom={true}
            tileSize={512}
            fadeAnimation={false}
            nocrs={CRS.EPSG4326}
        >
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                nourl="http://mt3.google.com/vt/lyrs=r&x={x}&y={y}&z={z}"
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            
            {/* <WMSTileLayer
                opacity={.15}
                transparent={true}
                
                url='http://ows.mundialis.de/services/service?'
                layers='SRTM30-Colored-Hillshade'
            /> */}

            <Circle
                center={position}
                radius={2000}
                fillColor='red'
                fillOpacity={.15}
                weight={3}
                color='red'
                opacity='.75'
            />

            <Polyline
                color='red'
                stroke={true}
                weight={5}
                opacity={.5}
                smoothFactor={21}
                positions={markers.sort((a, b) => b[0]>a[0]).map(position => position.position)}
            />

            { markers.length > 0 && markers.map((marker, i) => (
                <Marker
                    key={i}
                    position={[marker.position[0], marker.position[1]]}
                    draggable={false}
                    icon={new Icon({
                        iconSize: [30, 40],
                        iconAnchor: [15, 40],
                        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg',
                        popupAnchor: [0, -40]
                    })}
                />
            ))}

            <Marker
                position={position}
                draggable={false}
                icon={new Icon({
                    iconSize: [30, 40],
                    iconAnchor: [15, 40],
                    iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg',
                    popupAnchor: [0, -40]
                })}
            >
                <Tooltip>
                    This is the tooltip
                </Tooltip>

                <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                </Popup>

            </Marker>

        </MapContainer>

    )
}

export default Map;
